using DG.Tweening.Core.Easing;
using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.SceneManagement;

public class SceneLoader : MonoBehaviour
{
    public void LoadSceneNoTrans(string sceneName)
    {
        Time.timeScale = 1;
        PauseMenu.isPausing = false;
        SceneManager.LoadScene(sceneName);
    }

    public void LoadScene(string sceneName)
    {
        Time.timeScale = 1;
        PauseMenu.isPausing = false;
        TransitionManager.Instance.CallTransition(sceneName, 0);
    }

    public void LoadNextScene()
    {
        int number;
        PauseMenu.isPausing = false;
        int.TryParse(SceneManager.GetActiveScene().name.Split('_')[1],out number) ;
        
        Time.timeScale = 1;
        TransitionManager.Instance.CallTransition("Level_"+ (number + 1), 0);
    }

    public void LoadMainMenu()
    {
        Time.timeScale = 1;
        PauseMenu.isPausing = false;
        TransitionManager.Instance.CallTransition("MainMenu", 0);
    }
}
