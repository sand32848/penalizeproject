using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class InputController : Singleton<InputController>
{
    private PlayerAction playerAction;
    public PlayerAction PlayerAction
    {
        get
        {
            if (playerAction == null)
                playerAction = new PlayerAction();
            playerAction.Enable();
            return playerAction;
        }
    }

    private void Awake()
    {
        DontDestroyOnLoad(this);

        if (Instance != this)
        {
            Destroy(gameObject);
        }
    }
}
