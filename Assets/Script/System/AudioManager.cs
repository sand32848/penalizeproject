using System;
using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.Audio;
using DG.Tweening;
public class AudioManager : Singleton<AudioManager>
{
    private AudioSource currentMusic;
    public AudioMixer audioMixer;
    public Sound[] sounds;

    private void Awake()
    {
        DontDestroyOnLoad(this);

        if (Instance != this)
        {
            Destroy(gameObject);
        }

        foreach (Sound s in sounds)
        {
            s.audioSource = gameObject.AddComponent<AudioSource>();
            s.audioSource.clip = s.audioClip;
            s.audioSource.volume = s.volume;
            s.audioSource.pitch = s.pitch;
            s.audioSource.loop = s.loop;
            s.audioSource.outputAudioMixerGroup = audioMixer.outputAudioMixerGroup;
        }
    }

    public void Play(string name)
    {
       Sound s = Array.Find(sounds, sound => sound.name == name);
        if (s == null)
        {
            print("Sound not found");
            return;
        }
        s.audioSource.pitch = s.pitch;
        s.audioSource.Play();
        
    }

    public void PlayerRandomPitch(string name,float minPitch,float maxPitch)
    {
        Sound s = Array.Find(sounds, sound => sound.name == name);
        if (s == null)
        {
            print("Sound not found");
            return;
        }

        s.audioSource.pitch = UnityEngine.Random.Range(minPitch, maxPitch);
        s.audioSource.Play();
    }

    public void PlayMusic(string name)
    {
        Sound s = Array.Find(sounds, sound => sound.name == name);
        if(s== null)
        {
            print("Sound not found");
            return;
        }

        if (!currentMusic)
        {
            currentMusic = s.audioSource;
            currentMusic.volume = 0;
            currentMusic.DOFade(s.volume, 1f);
            s.audioSource.Play();
        }
        else
        {
            if (currentMusic == s.audioSource) return;

             currentMusic.DOFade(0, 1f).OnComplete(() => 
            {
                currentMusic.Stop();
                currentMusic = s.audioSource;
                currentMusic.DOFade(s.volume, 1f);
                s.audioSource.Play();
            });
        }
    }
}

[System.Serializable]
public class Sound
{
    public string name;
    public AudioClip audioClip;
    [Range(0f, 1f)]
    public float volume;
    [Range(0f, 3f)]
    public float pitch;
    public bool loop;
    [HideInInspector]
    public AudioSource audioSource;

}
