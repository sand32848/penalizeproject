using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class TransitionManager : Singleton<TransitionManager>
{
    [SerializeField] private CanvasGroup canvasGroup;
    private bool functionCall = false;
    private bool fade;
    private void Awake()
    {
        DontDestroyOnLoad(this);

        if (Instance != this)
        {
            Destroy(gameObject);
        }
    }

    private void OnEnable()
    {
        SceneManager.sceneLoaded += OnSceneLoaded;
    }

    private void OnDisable()
    {
        SceneManager.sceneLoaded -= OnSceneLoaded;

    }

    public void CallTransition(string sceneName, float sceneDelayTime, bool _fade = false,float fadeIn = 1,float fadeOut =1)
    {
        functionCall = true;

        fade = _fade;

        if (fade)
        {
            var transitionSequence = DOTween.Sequence();
            transitionSequence.Append(canvasGroup.GetComponent<RectTransform>().DOLocalMoveX(0, 0f));
            transitionSequence.Append(canvasGroup.DOFade(1, 0.5f));
            transitionSequence.Append(canvasGroup.DOFade(1, sceneDelayTime).OnComplete(() => { SceneManager.LoadScene(sceneName); }));
        }
        else
        {
            var transitionSequence = DOTween.Sequence();
            transitionSequence.Append(canvasGroup.GetComponent<RectTransform>().DOLocalMoveX(0, 0.5f)).SetEase(Ease.Linear);
            transitionSequence.Append(canvasGroup.DOFade(1, sceneDelayTime).OnComplete(() => { SceneManager.LoadScene(sceneName); }));
        }

    }

    void OnSceneLoaded(Scene scene, LoadSceneMode mode)
    {
        if (!functionCall) return;

        if (fade)
        {
            canvasGroup.DOFade(0, 1f).OnComplete(() => canvasGroup.GetComponent<RectTransform>().DOLocalMoveX(-800, 0f));
        }
        else
        {
            canvasGroup.GetComponent<RectTransform>().DOLocalMoveX(800, 0.5f).SetEase(Ease.Linear).OnComplete(() => canvasGroup.GetComponent<RectTransform>().DOLocalMoveX(-800, 0f));
        }
       
        functionCall = false;
        Time.timeScale = 1f;
    }
}
