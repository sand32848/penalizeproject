using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class Encylopedia : MonoBehaviour
{
    [SerializeField] private GameObject encylopedia;
    private void OnEnable()
    {
        InputController.Instance.PlayerAction.Player.Encylopedia.started += ToggleEncylopedia;
    }

    private void OnDisable()
    {
        InputController.Instance.PlayerAction.Player.Encylopedia.started -= ToggleEncylopedia;

    }

    public void ToggleEncylopedia(InputAction.CallbackContext context)
    {
        encylopedia.SetActive(!encylopedia.activeSelf);
    }

    public void CloseEncylopedia()
    {
        encylopedia.SetActive(false);
    }
}
