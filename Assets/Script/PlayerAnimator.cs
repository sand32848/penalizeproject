using System.Collections;
using System.Collections.Generic;
using System.Runtime.CompilerServices;
using UnityEngine;
using UnityEngine.InputSystem;

public class PlayerAnimator : MonoBehaviour
{
    [SerializeField] private Animator animator;
    [SerializeField] private ParticleSystem walkParticle;

    private float currentMovement;

    private void OnEnable()
    {

        InputController.Instance.PlayerAction.Player.Movement.started += PlayWalkParticle;
        InputController.Instance.PlayerAction.Player.Movement.canceled += PlayWalkParticle;
    }

    private void OnDisable()
    {

        InputController.Instance.PlayerAction.Player.Movement.started -= PlayWalkParticle;
        InputController.Instance.PlayerAction.Player.Movement.canceled -= PlayWalkParticle;

    }

    private void Update()
    {
        if (currentMovement != 0)
        {
            animator.Play("Walk");
            walkParticle.transform.localScale = new Vector3(-1, 1, 1);
        }
        else
        {
            animator.Play("Idle");
            walkParticle.transform.localScale = new Vector3(1, 1, 1);  
        }
    }

    public void PlayWalkParticle(InputAction.CallbackContext context)
    {
        currentMovement = context.ReadValue<Vector2>().x;

        if (currentMovement != 0)
        {
            walkParticle.Play();
        }
        else
        {
            walkParticle.Stop();
        }
   
    }
}
