using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[System.Serializable]
public class SaveData {

    public Dictionary<int, bool> LevelData = new Dictionary<int, bool>();
}
