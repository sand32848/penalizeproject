using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticleController : MonoBehaviour
{
    [SerializeField] private SpriteRenderer directionSprite;
    public void ChangeParticleDirection(int direction)
    {
        directionSprite.gameObject.SetActive(true);

        if(direction == 1)
        {
            directionSprite.flipX= false;
        }
        else
        {
            directionSprite.flipX = true;
        }
    }
}
