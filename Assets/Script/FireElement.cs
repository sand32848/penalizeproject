using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireElement : MonoBehaviour
{
    [SerializeField] private int index;
    public static Action<int> DestroyTile;

    public void InvokeDestoryTile()
    {
        DestroyTile?.Invoke(index);
    }
}
