
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class PlayerDirection : MonoBehaviour
{
    [SerializeField] private SpriteRenderer spriteRenderer;
    public float currentDirection { get; private set; }
    private void OnEnable()
    {
        InputController.Instance.PlayerAction.Player.Movement.performed += ChangeSpriteDirection;
    }

    private void OnDisable()
    {
        InputController.Instance.PlayerAction.Player.Movement.performed -= ChangeSpriteDirection;
    }

    public void ChangeSpriteDirection(InputAction.CallbackContext context)
    {
        if (PauseMenu.isPausing) return;

        currentDirection = context.ReadValue<Vector2>().x;

        if(currentDirection >= 0.1)
        {
            spriteRenderer.flipX= false;
        }
        else if(currentDirection <= -0.1)
        {
            spriteRenderer.flipX = true;
        }
    }
}
