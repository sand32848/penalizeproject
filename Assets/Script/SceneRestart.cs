using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;
using UnityEngine.SceneManagement;

public class SceneRestart : MonoBehaviour
{
    private void OnEnable()
    {
        InputController.Instance.PlayerAction.Player.ReloadScene.started += OnRestart;
        InputController.Instance.PlayerAction.Player.Debug.started += OnDebug;
    }

    private void OnDisable()
    {
        InputController.Instance.PlayerAction.Player.ReloadScene.started -= OnRestart;
        InputController.Instance.PlayerAction.Player.Debug.started -= OnDebug;
    }

    public void OnRestart(InputAction.CallbackContext ctx)
    {
        ReloadScene();
    }
    public void OnDebug(InputAction.CallbackContext ctx)
    {
        ReloadDebug();
    }
    public void ReloadScene()
    {
        Time.timeScale = 1;
        TransitionManager.Instance.CallTransition(SceneManager.GetActiveScene().name, 0);
    }

    public void ReloadDebug()
    {
        Time.timeScale = 1;
        SceneManager.LoadScene(SceneManager.GetActiveScene().name);
    }
}
