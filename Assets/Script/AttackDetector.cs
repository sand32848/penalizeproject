using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class AttackDetector : MonoBehaviour
{
    [SerializeField] private UnityEvent onHitEvent;
   public void OnHit()
    {
        onHitEvent.Invoke();
    }
}
