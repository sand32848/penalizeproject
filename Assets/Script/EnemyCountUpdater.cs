using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyCountUpdater : MonoBehaviour
{

    public static Action onDeath;

    public void InvokeOnDeath()
    {
        onDeath?.Invoke();
    }
}
