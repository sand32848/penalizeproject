using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class LevelSelectInitializer : MonoBehaviour
{
    [SerializeField] private GameObject buttonHolder;
    [SerializeField] private SceneLoader sceneLoader;

    private void Start()
    {
        InitializeSave();
    }

    [ContextMenu("ResetSave")]
    public void ResetSave()
    {
        SaveData saveData = new SaveData();
        SaveManager.SaveGameState(saveData);
    }

    public void InitializeSave()
    {
        Button[] buttonArray = buttonHolder.GetComponentsInChildren<Button>();

        SaveData saveData = SaveManager.LoadGameState();

        for (int i = 0; i < buttonArray.Length; i++)
        {
            TextMeshProUGUI text = buttonArray[i].GetComponentInChildren<TextMeshProUGUI>();
            text.text = (i + 1).ToString();


            if (saveData != null)
            {
                if (saveData.LevelData.ContainsKey(i) && saveData.LevelData[i] == true)
                {
                    if (buttonArray[i + 1])
                    {
                        buttonArray[i + 1].interactable = true;
                    }
                }
            }

            buttonArray[i].onClick.AddListener(() =>
            {
                sceneLoader.LoadScene("Level_" + text.text);
            });
        }

        buttonArray[0].interactable = true;
    }
}
