using JetBrains.Annotations;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class PlayerMovement : MonoBehaviour
{
    [SerializeField] private Rigidbody2D rigidbody2D;
    [SerializeField] private float HortizontalSpeed;
    [SerializeField] private float JumpForce;
    [SerializeField] private float Gravity;
    private float VerticalSpeed;
    private float moveDirY;
    private float moveDirX;

    private void OnEnable()
    {
        InputController.Instance.PlayerAction.Player.Movement.performed += OnMovmenet;
        InputController.Instance.PlayerAction.Player.Movement.canceled += OnMovmenet;
        InputController.Instance.PlayerAction.Player.Movement.canceled += OnJump;
    }

    private void OnDisable()
    {
        InputController.Instance.PlayerAction.Player.Movement.performed -= OnMovmenet;
        InputController.Instance.PlayerAction.Player.Movement.canceled -= OnMovmenet;
        InputController.Instance.PlayerAction.Player.Movement.canceled -= OnJump;
    }

    public void OnMovmenet(InputAction.CallbackContext context)
    {
        moveDirX = context.ReadValue<Vector2>().x;
    }

    public void OnJump(InputAction.CallbackContext context)
    {
        moveDirY += 5;
    }


    private void FixedUpdate()
    {
        rigidbody2D.velocity = new Vector2(moveDirX * HortizontalSpeed, rigidbody2D.velocity.y);
    }
}
