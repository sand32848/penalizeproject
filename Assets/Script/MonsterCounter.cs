using System;
using System.Collections;
using System.Collections.Generic;

using UnityEngine;
using UnityEngine.SceneManagement;

public class MonsterCounter : MonoBehaviour
{
    private int targetMonsterCount;
    private int currentMonsterCount;

    public static Action<int, int> OnUpdateMonsterCount;
    public static Action OnWin;


    private void OnEnable()
    {
        EnemyCountUpdater.onDeath += DecreaseMonsterCount;  
    }

    private void OnDisable()
    {
        EnemyCountUpdater.onDeath -= DecreaseMonsterCount;

    }

    private void Start()
    {
        InitiateMonsterCount();
        UpdateMonsterCount();
    }

    public void InitiateMonsterCount()
    {
        int count = GameObject.FindGameObjectsWithTag("Enemy").Length;
        targetMonsterCount = count;
    }

    public void UpdateMonsterCount()
    {
        OnUpdateMonsterCount?.Invoke(currentMonsterCount,targetMonsterCount);
    }

    public void DecreaseMonsterCount()
    {
        currentMonsterCount += 1;

        UpdateMonsterCount();

        if(currentMonsterCount >= targetMonsterCount)
        {
            int number;
            int.TryParse(SceneManager.GetActiveScene().name.Split('_')[1], out number);

            SaveData saveData = SaveManager.LoadGameState();

            if (saveData != null)
            {
                if (!saveData.LevelData.ContainsKey(number - 1))
                {
                    saveData.LevelData.Add(number - 1, true);
                }
              
            }
            else
            {
                saveData = new SaveData();
            }

            SaveManager.SaveGameState(saveData);

            OnWin?.Invoke();
        }
    }
}
