using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StatusApplier : MonoBehaviour
{
    [SerializeField] private List<StatusStat> statusStats= new List<StatusStat>();

    public static Action<StatusStat, int> OnApplyStatus;

    public void ApplyStatus()
    {
        foreach(StatusStat stat in statusStats)
        {
            OnApplyStatus?.Invoke(stat, stat.value);
        }
    }
}
