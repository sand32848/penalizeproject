using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.InputSystem;

public class PlayerAttack : MonoBehaviour
{
    [SerializeField] UnityEvent AttackEvent;
    [SerializeField] private float boxDistance;
    [SerializeField] private Vector2 colliderSize;
    [SerializeField] private PlayerDirection playerDirection;
    [SerializeField] private ParticleSystem particleSystem;
    private void OnEnable()
    {
        InputController.Instance.PlayerAction.Player.Attack.started += OnAttack;
    }

    private void OnDisable()
    {
        InputController.Instance.PlayerAction.Player.Attack.started -= OnAttack;
    }

    public void OnAttack(InputAction.CallbackContext context)
    {
        AudioManager.Instance.PlayerRandomPitch("Attack", 0.7f, 1.3f);

        var ps = particleSystem.main;
        var shape = particleSystem.shape;
        if (playerDirection.currentDirection <= -0.1)
        {
            
            ps.startRotation = Mathf.Deg2Rad * 180;
            shape.rotation = new Vector3(0, 180, 0);
        }
        else
        {
            ps.startRotation = 0;
            shape.rotation = new Vector3(0, 0, 0);
        }

        particleSystem.Play();

        AttackEvent.Invoke();

        Collider2D[] collider = (Physics2D.OverlapBoxAll(new Vector2(transform.position.x + ( boxDistance * Mathf.Ceil(playerDirection.currentDirection) ), transform.position.y), colliderSize, 0));

        foreach(Collider2D c in collider)
        {
            if(c.TryGetComponent(out AttackDetector attackDetector))
            {
                attackDetector.OnHit();
            }
        }
    }

    private void OnDrawGizmos()
    {
        Gizmos.color = Color.red;
        Gizmos.DrawWireCube(new Vector2(transform.position.x + (boxDistance * Mathf.Ceil(playerDirection.currentDirection)), transform.position.y), colliderSize);
    }
}
