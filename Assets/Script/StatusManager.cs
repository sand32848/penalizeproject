using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class StatusManager : MonoBehaviour
{
    [SerializeField] private List<StatusStat> statusList = new List<StatusStat>();

    private void OnEnable()
    {
        StatusApplier.OnApplyStatus += ModifiedStatus;
    }

    private void OnDisable()
    {
        StatusApplier.OnApplyStatus -= ModifiedStatus;
    }

    public void ModifiedStatus(StatusStat statusStat,int value)
    {
        StatusStat stat = statusList.Find(x => x.statusSO == statusStat.statusSO);
        stat.onApply.Invoke(value);
        stat.value = value;
    }

    public int GetStatus(string statusName)
    {
       StatusStat stat = statusList.Find(x => x.statusSO.statusName == statusName);

        return stat.value;
    }
}

[System.Serializable]
public class StatusStat
{
    [field:SerializeField] public StatusSO statusSO { get; private set; }
    [SerializeField] public int value;
    [SerializeField] public UnityEvent<int> onApply;
}
