using DG.Tweening;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WinUI : MonoBehaviour
{
    [SerializeField] private RectTransform winUI;

    private void OnEnable()
    {
        MonsterCounter.OnWin += ShowWinUI;
    }

    private void OnDisable()
    {
        MonsterCounter.OnWin -= ShowWinUI;
    }

    public void ShowWinUI()
    {
        winUI.gameObject.SetActive(true);
        winUI.DOLocalMoveY(0, 1f);
    }
}
