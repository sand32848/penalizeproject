using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;

public class LevelInitializer : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI stageName;
    private void Start()
    {
        AudioManager.Instance.PlayMusic("GamePlayTheme");

        int number;
        int.TryParse(SceneManager.GetActiveScene().name.Split('_')[1], out number);

        stageName.text = "STAGE-" + number;
    }
}
