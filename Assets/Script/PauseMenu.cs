using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.InputSystem;

public class PauseMenu : MonoBehaviour
{
    [SerializeField] private GameObject pauseMenu;
    public static bool isPausing;

    private void OnEnable()
    {
        InputController.Instance.PlayerAction.Player.Pause.started += pause;
    }

    private void OnDisable()
    {
        InputController.Instance.PlayerAction.Player.Pause.started -= pause;
    }

    public void CancleAll()
    {
        pauseMenu.SetActive(false);
     //   optionMenu.SetActive(false);
    }

    public void CallMenu()
    {
        pauseMenu.SetActive(true);
      //  generalMenu.SetActive(true);
    }

    public  void pause(InputAction.CallbackContext context)
    {
        if (!isPausing)
        {

            Time.timeScale = 0;
            CallMenu();
        }
        else
        {

            Time.timeScale = 1;
            CancleAll();
        }

        isPausing = !isPausing;
    }

    public void UnPause()
    {
        Time.timeScale = 1;
        CancleAll();

        isPausing = !isPausing;

    }
}
