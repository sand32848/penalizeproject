using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEngine;
using UnityEngine.EventSystems;
using UnityEngine.InputSystem;
using UnityEngine.UI;

public class ButtonSelector : MonoBehaviour
{
    [SerializeField] private List<Button> buttonList = new List<Button>();
    [SerializeField] private Color highLightColor;
    [SerializeField] private bool enable;
    [SerializeField] private int RowAmount = 1;
    [SerializeField] private int columnAmount = 1;
    [SerializeField] private List<ButtonColumnData> buttonRow = new List<ButtonColumnData>();
    [SerializeField] private Button backButton;

    private int buttonIndex = 0;
    private int buttonColumnIndex = 1;
    private int buttonRowIndex = 1;
    private int previousButtonIndex;

    private void OnEnable()
    {
        InputController.Instance.PlayerAction.Player.ButtonSelect.performed += SelectButton;
        InputController.Instance.PlayerAction.Player.ButtonConfirm.started += InvokeButton;
    }

    private void OnDisable()
    {
        InputController.Instance.PlayerAction.Player.ButtonSelect.performed -= SelectButton;
        InputController.Instance.PlayerAction.Player.ButtonConfirm.started -= InvokeButton;
    }

    private void Start()
    {
        ChangeButtonColor(buttonList[0], Color.yellow);
        enableButton();

        //Setup Column
        int buttonCount = 0;

        for (int i = 1; i < RowAmount+1; i++)
        {
            for (int j = 1;j< columnAmount + 1; j++)
            {
                ButtonColumnData button = new ButtonColumnData(j, i, buttonList[buttonCount]);
                buttonCount++;
                buttonRow.Add(button);

                if (buttonCount >= buttonList.Count) break;
            }
        }

        //Assign event
        foreach (Button b in buttonList)
        {
            ColorBlock color = b.colors;
            color.highlightedColor = Color.yellow;
            b.colors = color;

            EventTrigger trigger = b.GetComponent<EventTrigger>();
            EventTrigger.Entry entry = new EventTrigger.Entry();
            entry.eventID = EventTriggerType.PointerEnter;
           
            entry.callback.AddListener((eventData) => 
            {
                foreach (Button button in buttonList)
                {
                    ChangeButtonColor(button, Color.white);
                }

                if (b.interactable) {
                    ChangeIndex(b);
                    ChangeButtonColor(b, Color.yellow);
                    ButtonColumnData buttonColumnData = buttonRow.Find(a => a.Button == b);
                    buttonColumnIndex = buttonColumnData.Column;
                    buttonRowIndex = buttonColumnData.Row;
                }
            });

            trigger.triggers.Add(entry);
        }
       
    }

    public void SelectButton(InputAction.CallbackContext callbackContext)
    {
        if (!enable) return;

        int newRow = buttonRowIndex;
        int newColumn = buttonColumnIndex;

        if (callbackContext.ReadValue<Vector2>().x < 0)
        {
            newColumn = buttonColumnIndex - 1;
        }
        else if (callbackContext.ReadValue<Vector2>().x > 0)
        {
            newColumn= buttonColumnIndex + 1;
        }
        else if (callbackContext.ReadValue<Vector2>().y < 0)
        {
            newRow =  buttonRowIndex + 1;

            Button checkSkip = null;

            if (backButton)
            {
                if (buttonRow.Any(x => x.Column == newColumn && x.Row == newRow))
                {
                    checkSkip = buttonRow.Find(x => x.Column == newColumn && x.Row == newRow).Button;
                }

                if ((checkSkip && !checkSkip.interactable) || newRow == RowAmount)
                {
                    foreach (Button button in buttonList)
                    {
                        ChangeButtonColor(button, Color.white);
                    }

                    previousButtonIndex = buttonIndex;
                    ChangeButtonColor(backButton, Color.yellow);
                    ChangeIndex(backButton);

                    return;
                }
            }
 
        }
        else if (callbackContext.ReadValue<Vector2>().y > 0)
        {
            if(buttonIndex == buttonList.FindIndex(x => x == backButton))
            {
                print(previousButtonIndex);
                ChangeButtonColor(backButton, Color.white);
                ChangeButtonColor(buttonList[previousButtonIndex], Color.yellow);
                ChangeIndex(buttonList[previousButtonIndex]);

                return;
            }
            else
            {
                newRow = buttonRowIndex - 1;
            }
            
        }

        newColumn = Mathf.Clamp(newColumn, 1, columnAmount);
        newRow = Mathf.Clamp(newRow, 1, RowAmount);

        Button testButton = buttonRow.Find(x => x.Column == newColumn && x.Row == newRow).Button;

        if (!testButton.interactable) return;

        //confirm no skip
        buttonColumnIndex = newColumn;
        buttonRowIndex = newRow;

        foreach (Button button in buttonList)
        {
            ChangeButtonColor(button,Color.white);
        }

        Button selectButton = buttonRow.Find(x => x.Column == buttonColumnIndex && x.Row == buttonRowIndex).Button;

        ChangeButtonColor(selectButton,Color.yellow);
        ChangeIndex(selectButton);
    }

    public void ChangeIndex(Button button)
    {
        buttonIndex = buttonList.FindIndex(a => button == a);
    }

    public void ChangeButtonColor(Button button,Color _color)
    {
        ColorBlock color = button.colors;
        color.normalColor = _color;
        button.colors = color;
        button.GetComponentInChildren<TextMeshProUGUI>().color = _color;
    }

    public void InvokeButton(InputAction.CallbackContext callbackContext)
    {
        if (!enable) return;
        buttonList[buttonIndex].Select();
    }

    public void enableButton()
    {
        StartCoroutine(waitButton());
    }

    public IEnumerator waitButton()
    {
        yield return new WaitForSecondsRealtime(0.2f);

        enable = true;
    }
}

[System.Serializable]
public class ButtonColumnData
{
    public int Column;
    public int Row;
    public Button Button;

    public ButtonColumnData(int column, int row, Button button)
    {
        Column = column;
        Row = row;
        Button = button;
    }
}
