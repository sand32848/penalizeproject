
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EnemyAudio : MonoBehaviour
{
    [SerializeField] private List<string> audioName;

    public void playAudio()
    {
        int random = Random.Range(0,audioName.Count);

        AudioManager.Instance.Play(audioName[random]);
    }
}
