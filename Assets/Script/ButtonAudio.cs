using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ButtonAudio : MonoBehaviour
{
    Button button;
    private void Start()
    {
        button= GetComponent<Button>();

        button.onClick.AddListener(() => { AudioManager.Instance.Play("Button"); });
    }
}
