using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class MonsterCountUI : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI text;
    private void OnEnable()
    {
        MonsterCounter.OnUpdateMonsterCount += UpdateMonsterUI;
    }

    private void OnDisable()
    {
        MonsterCounter.OnUpdateMonsterCount -= UpdateMonsterUI;

    }

    public void UpdateMonsterUI(int curreMonster,int targetMonster)
    {
        text.text = curreMonster + "/" + targetMonster;
    }
}
