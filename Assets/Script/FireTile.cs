using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FireTile : MonoBehaviour
{
    [SerializeField] private int tileIndex;
    [SerializeField] private ParticleSystem particleSystem;
    [SerializeField] private SpriteRenderer spriteRenderer;
    [SerializeField] private Collider2D collider2D;

    private bool disable = false;

    private void OnEnable()
    {
        FireElement.DestroyTile += DestroyTile;
    }

    private void OnDisable()
    {
        FireElement.DestroyTile -= DestroyTile;
    }

    public void DestroyTile(int index)
    {
        if (index != tileIndex) return;
        if (disable) return;
        disable = true;

        particleSystem.Play();
        collider2D.enabled = false;
        spriteRenderer.enabled = false;
    }
}
