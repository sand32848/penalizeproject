using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class JumpDebugger : MonoBehaviour
{
    [SerializeField] private GameObject jumpDebugger;

    public void spawnDebug()
    {
        Instantiate(jumpDebugger,transform.position,Quaternion.identity);
    }
}
