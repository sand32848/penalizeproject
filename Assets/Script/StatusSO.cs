using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu(fileName = "Status", menuName = "Status", order = 1)]
public class StatusSO : ScriptableObject
{
    [SerializeField] public string statusName { get; private set; }
}
