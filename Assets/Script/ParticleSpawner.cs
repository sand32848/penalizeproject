using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticleSpawner : MonoBehaviour
{
    [SerializeField] private ParticleSystem particleSystem;

    public void SpawnParticle()
    {
        Instantiate(particleSystem,transform.position,particleSystem.transform.rotation);
    }
}
